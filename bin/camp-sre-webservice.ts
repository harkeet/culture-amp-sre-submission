#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { CampSreWebserviceStack } from '../lib/camp-sre-webservice-stack';

const app = new cdk.App();
new CampSreWebserviceStack(app, 'CampSreWebserviceStack');
