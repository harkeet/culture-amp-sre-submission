## Coding Challenge 

Here we have simple API that updates a single item in a DynamoDB table and retreives the value.

# Your Task

Write the infrastructure as code that will stand up this API in AWS.
You may use any technique or tooling that you like.
We would like to see you tackle areas like observability, security and scalability.
Take it as far as you like, to the point where you would be happy going to production.

If you choose to use Lambda, you can alter the code as you like to get it to run.

## Application Description

The API has four endpoints

- `/` returns "Home"
- `/upsert` creates or updates an item
- `/show` returns the item
- `/error` throws an error

## Other information

Please include a Readme with any additional information you would like to include. You may wish to use it to explain any design decisions.

Despite this being a small API, please approach this as you would a production problem using whatever approach to coding and infrastructure you feel appropriate. Successful candidates will be asked to extend their implementation in a pair programming session as a component of the interview.

## Submitting your solution

Assuming you use Git, when you’re ready to submit your solution, please use `git bundle` to package up a copy of your repository (with complete commit history) as a single file and send it to us as an email attachment.

```
git bundle create sre-interview-submission.bundle master
```

We're looking forward to your innovative solutions!

---

## APPROACH 1 - CONVERTING THE GO CODE INTO SINGLE LAMBDA FUNCTION

Started with converting the Golang application as a simple web service hosted as lambda function with an API Gateway and DynamoDB backend.

![Single_lb](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/single_lambda.png)

1. Modified the code to receive and send response as API Gateway Proxy request events.
2. Tried using the request path from the API Gateway Proxy request as routing to server different endpoints but faced issue in routing the requests with a third party router.
3. Tried embedding the AWS adapter and Gorilla MUX router which receives an API Gateway proxy event, transforms it into an http.Request object, and sends it to the mux router handle function for routing. This approch needed some time to get the code working.

Code can be found on <> this folder in repo.

## Shortcoming for Approach 1
As the application add additional routes the Lambda function becomes more complex and deployments of new versions would require to replace the entire function. With this approach it also becomes harder for multiple developers to work on the same project.

---

## APPROACH 2 – DECOUPLING GO CODE INTO MULTIPLE LAMBDA FUNCTIONS
Using the multiple single purpose lambda functions gives you more flexibility in terms of development and deployment as developers from different domains can work on separate endpoints logics and also it becomes easier to reuse the code template. Using a specific Lambda function to server a specific use case can also gives you can option to restrict permissions depending on the use case.

![multi_lb](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/multi_lambda.png)

Solution is submitted using the above approach.

---
## PROPOSED ARCHITECTURE

![Proposed_Arch](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/camp_sub_arch.png)

AWS Service Used:

* [Lambda]
* API Gateway
* Cognito
* DynamoDB
* CloudWatch
---

## LAMBDA FUNCTIONS

Following are the 
* HomeHandler:
   * Endpoint: /
   * Request: ANY request
   * Input: Triggering the endpoint url
   * Output: Home Message
   * Authentication: Not set for root
* UpsertHandler:
   * Endpoint: /upsert
   * Request: POST request
   * Input: Client send the Movie json data structure.
   * Output: Item is added to DynamoDB table and message is returned.
   * Authentication: Cognito Authentication configured and requires Authentication header in the request containing valid ID_Token. 
*  GetItem:
   * Endpoint: /show
   * Request: GET request
   * Input: Client passes in the Movie Title and Year to be retrieved via endpoint.
   * Output: Movie data is outputted
   * Authentication: Cognito Authentication configured and requires Authentication header in the request containing valid ID_Token.
* ErrorHandler:
  * Endpoint: /error
  * Request: GET request
  * Input: Triggering the error endpoint
  * Output: Error message
  * Authentication: Cognito Authentication configured and requires Authentication header in the request containing valid ID_Token.

---

## Cognito Authentication Setup

API Gateway validates the tokens from a successful user pool authentication, and uses them to grant the users access to resources using id_token in API requests.

![Cognito](https://docs.aws.amazon.com/cognito/latest/developerguide/images/scenario-api-gateway.png)

After the API is deployed, the client must first sign the user in to the user pool, obtain an identity or access token for the user, and then call the API method with one of the tokens as Authorization header. The API call succeeds only if the required token is supplied and the supplied token is valid, otherwise, the client isn't authorized to make the call because the client did not have credentials that could be authorized.

---

### Choice of Infrastruce as Code (IaC)
Options:
1. Terraform

2. CDK (Typescript/Python)

3. Terraform for CDK (dumps into terraform json config instead of CF)

4. Serverless Framework (I used to setup a base sekeleton for testing the go code)

Choosed **CDK with TypeScript** language as I also wanted to take this exercide as a learning experience for Typescript.

--- 

### Prerequisites

To work with the AWS CDK, you must have an AWS account and credentials and have installed Node.js and the AWS CDK Toolkit. See AWS CDK Prerequisites.

You also need TypeScript itself. If you don't already have it, you can install it using npm.

```sh
$ npm install -g typescript
```

### CDK Installation
Install or update the [AWS CDK CLI] from npm (requires [Node.js ≥ 10.13.0](https://nodejs.org/download/release/latest-v10.x/)). We recommend using a version in [Active LTS](https://nodejs.org/en/about/releases/)
⚠️ versions `13.0.0` to `13.6.0` are not supported due to compatibility issues with our dependencies.

```console
$ npm i -g aws-cdk
```

## Useful commands

 * `cdk deploy`  deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template

## Clone the github repo

```console
$ git clone git-url.git
$ cd sre-interview-submission-harkeet/camp-sre-webservice
```

To synthesize a CDK app, use the cdk synth command. Let’s check out the template synthesized from the sample app:
cdk synth

Managing AWS Construct Library modules
Use the Node Package Manager (npm), included with Node.js, to install and update AWS Construct Library modules for use by the stack, as well as other packages you need. (You may use yarn instead of npm if you prefer.) npm also installs the dependencies for those modules automatically.

The AWS CDK core module is named @aws-cdk/core. AWS Construct Library modules are named like @aws-cdk/SERVICE-NAME. The service name has an a prefix.

Use the following command to install required modules

```console
$ npm install @aws-cdk/core @aws-cdk/aws-lambda @aws-cdk/aws-dynamodb @aws-cdk/aws-apigateway @aws-cdk/aws-cognito @aws-cdk/aws-cloudwatch
```
Project's dependencies are maintained in package.json.
```npm
npm update
```

Deploy this to your account:

```console
$ cdk deploy
```

Use the `cdk` command-line toolkit to interact with your project:

 * `cdk deploy`: deploys your app into an AWS account
 * `cdk synth`: synthesizes an AWS CloudFormation template for your app
 * `cdk diff`: compares your app with the deployed stack

Output at the end of the deployment gives you the following info-

![cdk_out](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/CDK_Output.png)

* API Endpoint URL
* User Pool ID
* User Pool Client ID
* User Pool OIDC URL (For Authenticating with test user and retreiving id_token for api requests)




---
### Authenticating with test API User-

1. Log into AWS Console.
2. Navigate to the AWS Cognito service.
3. Go to Manage User Pool.
4. Navigate to User Pool Created by CDK Stack.
5. Go to User and group.
6. Create User inside User Pool Created via CDK Stack .

![apiuser](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/create_user_oidc.png)

7. Then go to App Clinet and click the CDK App Client and note down the secret to be used later.

---
### Test user Verification

Using OpenID Connect Playground for user verification and id_token retrieval

1. Navigate to OpenID Connect Playground (https://openidconnect.net/).
2. Click on OpenID Connect Configuration.
3. Select Custom Server Template from configuration.
![oidc_config](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/oidc_configuration.png)
4. In Discovery Document URL provide the in the ouput of CDK Stack.
5. Click Use Discovery Document and respective fields would be auto-populated.
6. Provide the OIDC Client ID from the User Pool Client ID output.
7. Provide the OIDC Client Secret from the step before.
8. Ensure Scope contains only 'openid profile email'.
9. Click Save.

---
### Token ID and User Validation

1. Redirect to OpenID Connect Server and click start.

![step1](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/oidc_step1.png)

2. Enter the test username and temporary password.
3. Change the password and click send.

![ch_pswd](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/oidc_user_pwd.png)

4. Exchange Code from Token
5. Verify User Token

![id_token](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/id_token.png)

6. Valid User id_token would be used for API Endpoints requests.

---

### API Endpoint Requests

Since all endpoints apart from root requires authentication and valid id_token for interacting with backend resources.

Example shown below shows behaviour for /upsert endpoint.

1. Request without **`Authorization header`**.
![ep_unauth](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/postman_unauth.png)

2. Request with valid **`Authorization header`**.
![ep_auth](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/postman_auth.png)


Querying the `/show endpoint` for fetching Movie data using query string-

![show_ep](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/736d12ed2e11126c246256cea9a2699856608a2a/imagerefmd/Show_movie.jpg)


---

# CloudWatch Dashboard

### DynamoDB Metrics:-

| Metrics | Description |
| ------ | ------ |
| ConsumedReadCapacityUnits | The number of read capacity units consumed over the specified time period, so you can track how much of your provisioned throughput is used. |
| ConsumedWriteCapacityUnits | The number of write capacity units consumed over the specified time period, so you can track how much of your provisioned throughput is used. |
| DynamoDB operations Latency | DynamoDB operations: (PutItem, GetItem, UpdateItem)


---


### Lambda Metrics:-

| Metrics | Description |
| ------ | ------ |
| aws.lambda.duration.p50 | Measures the p50 elapsed wall clock time from when the function code starts executing as a result of an invocation to when it stops executing. |
| aws.lambda.duration.p99 | Measures the p99 elapsed wall clock time from when the function code starts executing as a result of an invocation to when it stops executing. |
| aws.lambda.duration.p90 | Measures the p90 elapsed wall clock time from when the function code starts executing as a result of an invocation to when it stops executing. |

---



![cloudwatch](https://bitbucket.org/harkeet/sre-interview-submission-harkeet/raw/9bee08d61ea381766bc69d64302b0b1febbaf29a/imagerefmd/CloudWatchDashboard.png)


