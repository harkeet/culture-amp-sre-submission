package main

import (
	"camp-lambda/api_response"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// Always returns an error
func Handler(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	return api_response.ApiResponse(http.StatusInternalServerError, "Returned Error!")
}

func main() {
	lambda.Start(Handler)
}
