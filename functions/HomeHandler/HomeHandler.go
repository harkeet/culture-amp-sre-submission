package main

import (
	"camp-lambda/api_response"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// homeHandler is a simple endpoint that could be used a a health check
func Handler(req events.APIGatewayProxyRequest) (
	*events.APIGatewayProxyResponse,
	error,
) {
	return api_response.ApiResponse(http.StatusOK, "Home")
}

func main() {
	lambda.Start(Handler)
}
