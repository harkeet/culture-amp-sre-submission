package main

import (
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

// Item is just a sample struct
type Movie struct {
	Year   int     `json:"Year"`
	Title  string  `json:"Title"`
	Plot   string  `json:"Plot"`
	Rating float64 `json:"Rating"`
}

// Added for Error received from DB operations
type ErrorBody struct {
	ErrorMsg *string `json:"error,omitempty"`
}

// getItem tries to get the item created in upsertItem
func Handler(req events.APIGatewayProxyRequest) (
	events.APIGatewayProxyResponse,
	error,
) {

	movieName := req.QueryStringParameters["Title"]

	dynaClient, err := newDynamodbClient()

	tableName, _ := os.LookupEnv("TABLE_NAME")

	payload := &dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"Title": {
				S: aws.String(movieName),
			},
		},
	}

	result, err := dynaClient.GetItem(payload)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: req.Body, StatusCode: 404}, err
	}

	if result.Item == nil {
		message := fmt.Sprintf("Could not find movie : " + movieName)
		return events.APIGatewayProxyResponse{Body: message, StatusCode: 404}, nil
	}

	item := new(Movie)

	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Failed to unmarshal Record", StatusCode: 404}, nil
	}

	message := fmt.Sprintf("Found Movie: Title: %+v Year: %+v Plot: %+v Rating: %+v ", item.Title, item.Year, item.Plot, item.Rating)
	return events.APIGatewayProxyResponse{Body: message, StatusCode: 200}, nil

}

// newDynamodbClient creates a client with the default aws session
func newDynamodbClient() (*dynamodb.DynamoDB, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}
	return dynamodb.New(sess), nil
}

func main() {
	lambda.Start(Handler)
}
