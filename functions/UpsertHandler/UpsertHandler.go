package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

// Item is just a sample struct
type Movie struct {
	Year   int     `json:"Year"`
	Title  string  `json:"Title"`
	Plot   string  `json:"Plot"`
	Rating float64 `json:"Rating"`
}

// Added for Error received from DB operations
type ErrorBody struct {
	ErrorMsg *string `json:"error,omitempty"`
}

// upsertHandler create or sets a single item in a dynamo db table
func Handler(req events.APIGatewayProxyRequest) (
	events.APIGatewayProxyResponse,
	error,
) {
	// result, err := movies.AddMovie(req, tableName, dynaClient)
	// fmt.Println("Received body: ", req.Body)
	// return events.APIGatewayProxyResponse{Body: req.Body, StatusCode: 200}, nil
	var item Movie
	tableName, _ := os.LookupEnv("TABLE_NAME")

	err := json.Unmarshal([]byte(req.Body), &item)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 404}, err
	}

	dynaClient, err := newDynamodbClient()

	av, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		fmt.Println("Got error marshalling new movie item:")
		fmt.Println(err.Error())
		os.Exit(1)
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}

	_, err = dynaClient.PutItem(input)
	if err != nil {
		fmt.Println("Got error calling PutItem:")
		fmt.Println(err.Error())
		//return api_response.ApiResponse(http.StatusInternalServerError, ErrorBody{aws.String(err.Error())})
		return events.APIGatewayProxyResponse{Body: req.Body, StatusCode: 404}, err
		os.Exit(1)
	}

	return events.APIGatewayProxyResponse{Body: "Added!", StatusCode: 200}, nil

}

// newDynamodbClient creates a client with the default aws session
func newDynamodbClient() (*dynamodb.DynamoDB, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}
	return dynamodb.New(sess), nil
}

func main() {
	lambda.Start(Handler)
}
