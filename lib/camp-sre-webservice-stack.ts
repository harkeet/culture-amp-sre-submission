import * as cdk from '@aws-cdk/core';
import * as lambda from "@aws-cdk/aws-lambda";
import * as dynamodb from "@aws-cdk/aws-dynamodb";
import * as apigw from "@aws-cdk/aws-apigateway";
import { UserPool, VerificationEmailStyle, UserPoolClient, AccountRecovery, UserPoolDomain, OAuthScope } from '@aws-cdk/aws-cognito';
// import sns = require('@aws-cdk/aws-sns');
import cloudwatch = require('@aws-cdk/aws-cloudwatch');
import { GraphWidget, IMetric, Metric } from "@aws-cdk/aws-cloudwatch";
// import { SnsAction } from '@aws-cdk/aws-cloudwatch-actions';

export class CampSreWebserviceStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // COGNITO ---> API GW ---> EP Lambda ---> CloudWatch Stack

    //Dynamodb table Creation to used by lambda functions
    const dytable = new dynamodb.Table(this, "CAmpSRETable", {
      partitionKey: { name: "Title", type: dynamodb.AttributeType.STRING },
    });

    // Lambda function for Home root handler
    const homeLambda = new lambda.Function(this, "HomeHandler", {
      runtime: lambda.Runtime.GO_1_X,
      code: lambda.Code.fromAsset("functions/bin/HomeHandler.zip"),
      handler: "HomeHandler",
      environment: {
        TABLE_NAME: dytable.tableName,
      },
    });

     // Lambda function for Error endpoint handler
    const errorLambda = new lambda.Function(this, "ErrorHandler", {
      runtime: lambda.Runtime.GO_1_X,
      code: lambda.Code.fromAsset("functions/bin/ErrorHandler.zip"),
      handler: "ErrorHandler",
      environment: {
        TABLE_NAME: dytable.tableName,
      },
    });

    // Lambda function for Upsert endpoint handler
    const upsertLambda = new lambda.Function(this, "UpsertHandler", {
      runtime: lambda.Runtime.GO_1_X,
      code: lambda.Code.fromAsset("functions/bin/UpsertHandler.zip"),
      handler: "UpsertHandler",
      environment: {
        TABLE_NAME: dytable.tableName,
      },
    });

    // Lambda function for Show endpoint handler
    const showLambda = new lambda.Function(this, "GetItem", {
      runtime: lambda.Runtime.GO_1_X,
      code: lambda.Code.fromAsset("functions/bin/GetItem.zip"),
      handler: "GetItem",
      environment: {
        TABLE_NAME: dytable.tableName,
      },
    });


    // Grant DynamoDB R/W persmissions to specific EP Lambda
    dytable.grantReadWriteData(upsertLambda);
    dytable.grantReadData(showLambda);

    const userPool = new UserPool(this, 'camp-cdk-user-pool', {
      selfSignUpEnabled: true,
      accountRecovery: AccountRecovery.PHONE_AND_EMAIL,
      userVerification: {
        emailStyle: VerificationEmailStyle.CODE
      },
      autoVerify: {
        email: true
      },
      standardAttributes: {
        email: {
          required: true,
          mutable: true
        }
      }
    });

    const userPoolClient = new UserPoolClient(this, "UserPoolClient", {
      userPool: userPool,
      generateSecret: true,
      oAuth: {
        callbackUrls: [
          'https://openidconnect.net/callback',
        ],
        flows: {
          authorizationCodeGrant: true,
          implicitCodeGrant: true,
        },
        scopes: [
          OAuthScope.EMAIL,
          OAuthScope.OPENID,
          OAuthScope.PROFILE
        ]
      }
    });

    const userPoolDomain = new UserPoolDomain(this, 'UserPoolDomain', {
      userPool: userPool,
      cognitoDomain: {
        domainPrefix: 'camp-cdk-domain'
      }
    })


    new cdk.CfnOutput(this, "UserPoolId", {
      value: userPool.userPoolId
    });
    
    new cdk.CfnOutput(this, "UserPoolClientId", {
      value: userPoolClient.userPoolClientId
    });

    new cdk.CfnOutput(this, "UserPoolIdOIDCUrl", {
      value: "https://cognito-idp."+ process.env.CDK_DEFAULT_REGION + ".amazonaws.com/"+ userPool.userPoolId + "/.well-known/openid-configuration"
    });

    // create the API Gateway with one method and path
    const api = new apigw.RestApi(this, "camp-cdk-ts-api");

    // Handling Authorization to API GW
    const authorizationHeaderName = "Authorization";

    const cfnAuthorizer = new apigw.CfnAuthorizer(this, id, {
      name: "CognitoAuthorizer",
      type: apigw.AuthorizationType.COGNITO,

      identitySource: "method.request.header." + authorizationHeaderName,
      restApiId: api.restApiId,
      providerArns: [userPool.userPoolArn]
    });
   
    api.root
    .resourceForPath("/")
    .addMethod("ANY", new apigw.LambdaIntegration(homeLambda));

    api.root
    .addResource("{proxy+}")
    .addMethod("ANY", new apigw.LambdaIntegration(errorLambda), {

      authorizer: {authorizerId: cfnAuthorizer.ref},
      authorizationType: apigw.AuthorizationType.COGNITO,

    });

    api.root
    .resourceForPath("show")
    .addMethod("GET", new apigw.LambdaIntegration(showLambda), {

      authorizer: {authorizerId: cfnAuthorizer.ref},
      authorizationType: apigw.AuthorizationType.COGNITO,

    });

    api.root
    .resourceForPath("error")
    .addMethod("GET", new apigw.LambdaIntegration(errorLambda), {

      authorizer: {authorizerId: cfnAuthorizer.ref},
      authorizationType: apigw.AuthorizationType.COGNITO,

    });

    api.root
    .resourceForPath("upsert")
    .addMethod("POST", new apigw.LambdaIntegration(upsertLambda), {

      authorizer: {authorizerId: cfnAuthorizer.ref},
      authorizationType: apigw.AuthorizationType.COGNITO,

    });


    // Monitoring of the WebService

    //SNS Topic for receiving events
    // const camperrorTopic = new sns.Topic(this, 'campTopic');

    // Upsert Endpoint calls error percentage in past 2 mins
    let upsertLambdaErrorPercentage = new cloudwatch.MathExpression({
      expression: 'e / i * 100',
      label: '% of invocations that errored, last 5 mins', 
      usingMetrics: {
        i: upsertLambda.metric("Invocations", {statistic: 'sum'}),
        e: upsertLambda.metric("Errors", {statistic: 'sum'}),
      },
      period: cdk.Duration.minutes(2)
    });

    // Show Endpoint calls error percentage in past 2 mins
    let showLambdaErrorPercentage = new cloudwatch.MathExpression({
      expression: 'e / i * 100',
      label: '% of invocations that errored, last 2 mins', 
      usingMetrics: {
          i: showLambda.metric("Invocations", {statistic: 'sum'}),
          e: showLambda.metric("Errors", {statistic: 'sum'}),
      },
      period: cdk.Duration.minutes(2)
      });

    
    // Culture Amp Web Service Dashboard

    new cloudwatch.Dashboard(this, 'CultureAmpCloudWatchDashBoard').addWidgets(
        this.buildGraphWidget('Upsert Lambda Error %', [upsertLambdaErrorPercentage]),
        
        // Lambda Metrics
        // https://docs.datadoghq.com/integrations/amazon_lambda/

        this.buildGraphWidget('Upsert Lambda Metrics', [
        upsertLambda.metricDuration({statistic:"p50"}),
        upsertLambda.metricDuration({statistic:"p90"}),
        upsertLambda.metricDuration({statistic:"p99"})
        ], true),
    
        this.buildGraphWidget('Show Lambda Error %', [showLambdaErrorPercentage]),

        this.buildGraphWidget('Show Lambda Metrics', [
        showLambda.metricDuration({statistic:"p50"}),
        showLambda.metricDuration({statistic:"p90"}),
        showLambda.metricDuration({statistic:"p99"})
        ], true),
    
        this.buildGraphWidget('DynamoDB Consumed Read/Write Units', [
            dytable.metric('ConsumedReadCapacityUnits'),
            dytable.metric('ConsumedWriteCapacityUnits')
            ], false),
    
        this.buildGraphWidget('DynamoDB Latency', [
            dytable.metricSuccessfulRequestLatency({dimensions: {"TableName":dytable.tableName, "Operation": "GetItemInput"}}),
            dytable.metricSuccessfulRequestLatency({dimensions: {"TableName":dytable.tableName, "Operation": "UpdateItem"}}),
            dytable.metricSuccessfulRequestLatency({dimensions: {"TableName":dytable.tableName, "Operation": "PutItem"}}),
            dytable.metricSuccessfulRequestLatency({dimensions: {"TableName":dytable.tableName, "Operation": "GetItem"}}),
            dytable.metricSuccessfulRequestLatency({dimensions: {"TableName":dytable.tableName, "Operation": "Query"}}),
        ], true)
    )


  }

  private buildGraphWidget(widgetName: string, metrics: IMetric[], stacked = false): GraphWidget {
    return new GraphWidget({
      title: widgetName,
      left: metrics,
      stacked: stacked,
      width: 8
    });    

  }  

}